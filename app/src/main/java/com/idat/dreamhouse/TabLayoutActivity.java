package com.idat.dreamhouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.idat.dreamhouse.adapter.VpAdapter;

public class TabLayoutActivity extends AppCompatActivity {
    TabLayout tl1;
    ViewPager2 vp;
    VpAdapter adapter;

    String[] titulos = new String[]{"Empleados","Usuarios","Edificios", "Departamentos", "Clientes", "Contratos"};

    int [] iconos= new int[] {R.drawable.ic_baseline_engineering_24,R.drawable.ic_usuarios, R.drawable.ic_edificios, R.drawable.ic_departamentos,
    R.drawable.ic_clientes, R.drawable.ic_contratos};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);

        Enlazar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.manu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i=null;
        switch (item.getItemId()){
            case R.id.mnmantenimiento:
                i= new Intent(TabLayoutActivity.this, TabLayoutActivity.class);
                break;
            case R.id.mncontactos:
                i= new Intent(TabLayoutActivity.this, ContactosActivity.class);
                break;
            case R.id.mnsalir: finishAndRemoveTask();
                i= new Intent(TabLayoutActivity.this, LoginActivity.class);
                break;
        }
        if (i!=null){
            startActivity(i);
        }
        return true;
    }

    void Enlazar(){
        tl1= findViewById(R.id.tb);
        vp= findViewById(R.id.vp1);

        adapter= new VpAdapter(
                getSupportFragmentManager(),getLifecycle()
        );

        vp.setAdapter(adapter);

        new TabLayoutMediator(tl1,vp,(
                (tab, position) -> tab.setText(titulos[position]).setIcon(iconos[position]))).attach();
    }
}