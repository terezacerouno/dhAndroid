package com.idat.dreamhouse.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Contrato;

import java.util.List;

@Dao
public interface ContratoDaodb {

    @Insert
    void create(Contrato contrato);

    @Update
    void update(Contrato contrato);

    @Query(value="delete from contrato where codcont= :id")
    void deleteById(Long id);

    @Query(value = "select * from contrato")
    List<Contrato> findAll();

    @Query(value = "select * from contrato where codcont= :id")
    Contrato findById(Long id);

    @Query(value = "select * from contrato where coddepa= :iddepa")
    Contrato findRepeat(Long iddepa);

    @Query(value = "select * from contrato where coddepa= :iddepa and codcont <> :idcont")
    Contrato findRepeatUpd(Long iddepa, Long idcont);

    @Query(value = "select * from contrato where codcont like '%' || :f|| '%' or coddepa like '%' || :f|| '%' or codcliente like '%' || :f|| '%' or garantia like '%' || :f|| '%'")
    List<Contrato> filtrado(String f);
}
