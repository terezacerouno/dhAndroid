package com.idat.dreamhouse.dao;

import com.idat.dreamhouse.model.Usuario;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


//No utilizar
public class UsuarioDao {

    public List<Usuario> ListarUsuarios(String cadena) throws JSONException {
        List<Usuario> usuarios= new ArrayList<>();

        JSONArray json= new JSONArray(cadena);

        for(int i=0; i<json.length();i++){
            Usuario u= new Usuario();
            u.setId_usuario(json.getJSONObject(i).getInt("id"));
            u.setEmail(json.getJSONObject(i).getString("email"));
            u.setPassword(json.getJSONObject(i).getString("password"));
            u.setEstado(json.getJSONObject(i).getString("estado"));
            u.setImagen(json.getJSONObject(i).getString("imagen"));

            usuarios.add(u);
        }
        return usuarios;
    }
}
