package com.idat.dreamhouse.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;

import java.util.List;

@Dao
public interface DepartamentoDaodb {

    @Insert
    void create(Departamento obj);

    @Update
    void update(Departamento obj);

    @Delete
    void delete(Departamento obj);

    @Query(value = "delete from Departamento where coddepa= :codigo")
    void deletecod(int codigo);

    @Query(value = "select * from Departamento")
    List<Departamento> listar();

    @Query(value = "select * from Departamento  where coddepa= :codigo")
    List<Departamento> getDepartamento(int codigo);

    @Query(value = "select * from Edificio e inner join Departamento d on e.codedificio=d.codedificio where e.codedificio= :codigo")
    List<Edificio> getEdificio(int codigo);

    @Query(value = "select * from Departamento where estadepa= 'Activo'")
    List<Departamento> custom();

    @Query(value = "select * from Departamento d inner join Edificio e on e.codedificio= d.codedificio where d.coddepa like '%' || :f|| '%' or d.estadepa like '%' || :f|| '%' or d.areadepa like '%' || :f|| '%' or d.banosdepa like '%' || :f|| '%' or d.habitacionesdepa like '%' || :f|| '%' or d.pisodepa like '%' || :f|| '%' or d.preciodepa like '%' || :f|| '%' or e.diredificio like '%' || :f|| '%'")
    List<Departamento> filtrado(String f);
}
