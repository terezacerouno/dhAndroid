package com.idat.dreamhouse.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;

import java.util.List;

@Dao
public interface EmpleadoDaodb {

    @Insert
    void create(Empleado obj);

    @Update
    void update(Empleado obj);

    @Delete
    void delete(Empleado obj);

    @Query(value = "delete from Empleado where codemple= :codigo")
    void deletecod(int codigo);

    @Query(value = "select * from Empleado")
    List<Empleado> listar();

    @Query(value = "select * from Empleado where codemple= :codigo")
    List<Empleado> getEmpleado(int codigo);

    @Query(value = "select * from Empleado where estemple= 'Activo'")
    List<Empleado> custom();

    @Query(value = "select * from Empleado where dniemple= :dni")
    List<Empleado> findByDni(int dni);

    @Query(value = "select * from Empleado where dniemple= :dni and codemple!= :codigo")
    List<Empleado> findByDni2(int codigo,int dni);

    @Query(value = "select * from Empleado where codemple like '%' || :f|| '%' or estemple like '%' || :f|| '%' or dniemple like '%' || :f|| '%' or amemple like '%' || :f|| '%' or apemple like '%' || :f|| '%' or nomemple like '%' || :f|| '%' or telemple like '%' || :f|| '%'")
    List<Empleado> filtrado(String f);
}
