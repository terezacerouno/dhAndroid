package com.idat.dreamhouse.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Usuario;

import java.util.List;

@Dao
public interface ClienteDaodb {

    @Insert
    void create(Cliente cliente);

    @Update
    void update(Cliente cliente);

    @Query(value="delete from cliente where codcliente= :id")
    void deleteById(Long id);

    @Query(value = "select * from cliente")
    List<Cliente> findAll();

    @Query(value = "select * from cliente where codcliente= :id")
    Cliente findById(Long id);

    @Query(value = "select * from cliente where dnicliente = :dni")
    Cliente findByDni(String dni);

    @Query(value = "select * from cliente where dnicliente = :dni and codcliente <> :id")
    Cliente findRepeat(String dni, Long id);

    @Query(value = "select * from Cliente where codcliente like '%' || :f|| '%' or dnicliente like '%' || :f|| '%' or nomcliente like '%' || :f|| '%' or apecliente like '%' || :f|| '%'")
    List<Cliente> filtrado(String f);
}
