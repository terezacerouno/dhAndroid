package com.idat.dreamhouse.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;
import com.idat.dreamhouse.model.Empleado;

import java.util.List;

@Dao
public interface EdificioDaodb {

    @Insert
    void create(Edificio obj);

    @Update
    void update(Edificio obj);

    @Delete
    void delete(Edificio obj);

    @Query(value = "delete from Edificio where codedificio= :codigo")
    void deletecod(int codigo);

    @Query(value = "select * from Edificio")
    List<Edificio> listar();

    @Query(value = "select *,MAX(codedificio) from Edificio")
    List<Edificio> Obtenerultimocodigo();

    @Query(value = "select * from Edificio  where codedificio= :codigo")
    List<Edificio> getEdicio(int codigo);

    @Query(value = "select * from Departamento d inner join Edificio e on e.codedificio=d.codedificio where d.codedificio= :codigo")
    List<Departamento> getDepartamentos(int codigo);

    @Query(value = "select * from Edificio where codedificio like '%' || :f|| '%' or desedificio like '%' || :f|| '%' or diredificio like '%' || :f|| '%' or pisosedificio like '%' || :f|| '%'")
    List<Edificio> filtrado(String f);
}
