package com.idat.dreamhouse.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;

import java.util.List;

@Dao
public interface UsuarioDaodb {

    @Insert
    void create(Usuario obj);

    @Update
    void update(Usuario obj);

    @Delete
    void delete(Usuario obj);

    @Query(value = "delete from Usuario where codus= :codigo")
    void deletecod(int codigo);

    @Query(value = "select * from Usuario")
    List<Usuario> listar();

    @Query(value = "select * from Usuario where codus= :codigo")
    List<Usuario> getUsuario(int codigo);

    @Query(value = "select * from Usuario where estus= 'Activo'")
    List<Usuario> custom();

    @Query(value = "select * from Usuario where codus like '%' || :f|| '%' or estus like '%' || :f|| '%' or emailus like '%' || :f|| '%'")
    List<Usuario> filtrado(String f);

    //Registrar
    @Query(value = "select * from Usuario where emailus= :email")
    List<Usuario> findByEmail(String email);

    //Actualizar
    @Query(value = "select * from Usuario where emailus= :email and codus!= :codigo")
    List<Usuario> findByEmail2(int codigo, String email);
}
