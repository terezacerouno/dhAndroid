package com.idat.dreamhouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.dao.UsuarioDao;
import com.idat.dreamhouse.model.Usuario;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText txtusuario, txtcontrasena;
    private TextInputLayout tilusuario, tilcontrasena;
    private Button btnRregistrar, btniniciarsesion;
    ConexionDb instancia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        instancia = ConexionDb.getInstancia(LoginActivity.this);


        EnlazarControles();
    }

    void EnlazarControles() {
        txtusuario = findViewById(R.id.txtusuario);
        txtcontrasena = findViewById(R.id.txtLcontrasena);
        btnRregistrar = findViewById(R.id.btnRregistrar);
        btniniciarsesion = findViewById(R.id.btniniciarsesion);
        tilusuario = findViewById(R.id.tilusuario);
        tilcontrasena = findViewById(R.id.tilcontra);

        btniniciarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Btniniciarsesion();
            }
        });
    }

    public void Btniniciarsesion() {
        ValidarDatos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtusuario.setText("");
        txtcontrasena.setText("");
        txtusuario.requestFocus();
    }

    void ValidarDatos() {
        tilusuario.setError("");
        tilusuario.setErrorEnabled(false);
        String usua = txtusuario.getText().toString().trim();

        tilcontrasena.setError("");
        tilcontrasena.setErrorEnabled(false);
        String contra = txtcontrasena.getText().toString();

        if (usua.equals("")) {
            tilusuario.setError("Debe ingresar un usuario");
            txtusuario.requestFocus();
        } else if (contra.equals("")) {
            tilcontrasena.setError("Debe ingresar una contraseña");
            txtcontrasena.requestFocus();
        } else if (usua.equals("admin") && contra.equals("admin")) {
            finish();
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        } else {
            try {
                Usuario usuario = instancia.dao().findByEmail(txtusuario.getText().toString()).get(0);
                if (usuario != null) {
                    if (usuario.getPassword().equals(txtcontrasena.getText().toString())) {
                        finish();
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                    } else {
                        tilcontrasena.setError("La contraseña es incorrecta");
                        tilcontrasena.requestFocus();
                    }
                }
            } catch (Exception e) {
                tilusuario.setError("El usuario no se encuentra registrado");
                tilusuario.requestFocus();
            }
        }
    }
}