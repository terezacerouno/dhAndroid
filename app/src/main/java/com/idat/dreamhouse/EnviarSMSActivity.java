package com.idat.dreamhouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.idat.dreamhouse.actualizar.ActualizarEmpleadoActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Empleado;

public class EnviarSMSActivity extends AppCompatActivity {

    TextView txtnumero;
    EditText txtMensaje;
    Button btnEnviar, btnCerrar;
    ConexionDb instancia;
    Empleado e;
    Integer codigo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_smsactivity);
        getSupportActionBar().hide();

        instancia= ConexionDb.getInstancia(EnviarSMSActivity.this);

        Intent x= getIntent();
        codigo= x.getIntExtra("CODIGO",0);
        e= instancia.daoempleado().getEmpleado(codigo).get(0);
        Enlazar();
    }

    void Enlazar(){
        txtnumero= findViewById(R.id.txtnumero);
        txtnumero.setText("Enviar mensaje a : "+e.getTelefono());
        txtMensaje= findViewById(R.id.txtMensaje);
        btnEnviar= findViewById(R.id.btnEnviarSms);
        btnCerrar= findViewById(R.id.btnCerrarSms);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmsManager sms= SmsManager.getDefault();
                sms.sendTextMessage(e.getTelefono(),null, txtMensaje.getText().toString(),null,null);
                Toast.makeText(EnviarSMSActivity.this, "Mensaje enviado a "+ e.getNombres() , Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}