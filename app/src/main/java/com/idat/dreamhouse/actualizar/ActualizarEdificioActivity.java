package com.idat.dreamhouse.actualizar;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.TabLayoutActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Edificio;
import com.idat.dreamhouse.nuevo.NuevoEdificioActivity;

public class ActualizarEdificioActivity extends AppCompatActivity {

    Button btnGrabar, btnNuevo;
    FloatingActionButton btnCerrar;
    TextInputLayout tildesc, tildire, tilnp;
    TextInputEditText txtdesc, txtdire, txtnp;
    ConexionDb instancia;
    int codigo;
    Edificio e;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_edificio);
        getSupportActionBar().hide();

        instancia= ConexionDb.getInstancia(ActualizarEdificioActivity.this);

        Intent x= getIntent();
        codigo= x.getIntExtra("CODIGO",0);
        e= instancia.daoedificio().getEdicio(codigo).get(0);

        Enlazar();

        txtdesc.setText(e.getDescripcion());
        txtdire.setText(e.getDireccion());
        txtnp.setText(""+e.getN_pisos());
    }

    void Enlazar(){
        tildesc= findViewById(R.id.tildescEdup);
        tildire= findViewById(R.id.tildireccionEdup);
        tilnp= findViewById(R.id.tilpisosEdup);
        txtdesc= findViewById(R.id.txtdescEdup);
        txtdire= findViewById(R.id.txtdireccionEdup);
        txtnp= findViewById(R.id.txtpisosEdup);


        btnNuevo= findViewById(R.id.btnlimpiarEdup);
        btnGrabar= findViewById(R.id.btnactualizarEd);
        btnCerrar= findViewById(R.id.btnsalirEdup);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actualizar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void Limpiar(){
        txtdesc.setText("");
        txtdire.setText("");
        txtnp.setText("");
        txtdire.requestFocus();
    }

    boolean ValidarDatos(){
        try {
            tildire.setError("");
            tildire.setErrorEnabled(false);
            String direccion = txtdire.getText().toString().trim();

            tildesc.setError("");
            tildesc.setErrorEnabled(false);
            String descripcion = txtdesc.getText().toString().trim();

            tilnp.setError("");
            tilnp.setErrorEnabled(false);

            if (direccion.equals("")){
                tildire.setError("Error, la dirección no puede estar vacía");
                txtdire.requestFocus();
                return false;
            }

            if (descripcion.equals("")){
                tildesc.setError("Error, la descripción no puede estar vacía");
                txtdesc.requestFocus();
                return false;
            }

            if (txtnp.getText().toString().equals("")){
                tilnp.setError("Error, el número de pisos no puede estar vacía");
                txtnp.requestFocus();
                return false;
            }else if (Integer.parseInt(txtnp.getText().toString())<1){
                tilnp.setError("Error, el número de pisos debe ser mayor a 0");
                txtnp.requestFocus();
                return false;
            }

            return true;
        }catch (Exception e){
            Toast.makeText(this, "Todos los datos son obligatorios", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    void Actualizar() {
        if (ValidarDatos()){
            ProgresoHorizontal();
        }
    }

    void ProgresoHorizontal(){
        ProgressDialog pd= new ProgressDialog(ActualizarEdificioActivity.this);

        pd.setTitle("Actualizar Edificio");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_upgrade_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        e.setDireccion(txtdire.getText().toString());
                        e.setDescripcion(txtdesc.getText().toString());
                        e.setN_pisos(Integer.parseInt(txtnp.getText().toString()));
                        instancia.daoedificio().update(e);
                        Toast.makeText(ActualizarEdificioActivity.this, "El edificio con id "+e.getId()+" se actualizó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
}