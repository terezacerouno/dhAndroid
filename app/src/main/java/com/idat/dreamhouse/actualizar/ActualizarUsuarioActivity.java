package com.idat.dreamhouse.actualizar;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.TabLayoutActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Usuario;
import com.idat.dreamhouse.nuevo.NuevoUsuarioActivity;

import java.util.regex.Pattern;

public class ActualizarUsuarioActivity extends AppCompatActivity {

    Button btnGrabar, btnNuevo;
    FloatingActionButton btnCerrar;
    TextInputLayout tilemail, tilpass, tilpasold;
    TextInputEditText txtemai, txtpassword, txtpasold;
    Spinner spnEstado;
    String [] estados={"Activo","Inactivo"};
    Usuario u;
    Integer codigo;
    ConexionDb instancia;
    Pattern pCorreo= Patterns.EMAIL_ADDRESS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_usuario);
        getSupportActionBar().hide();

        instancia= ConexionDb.getInstancia(ActualizarUsuarioActivity.this);

        Intent x= getIntent();
        codigo= x.getIntExtra("CODIGO",0);
        u= instancia.dao().getUsuario(codigo).get(0);
        Enlazar();
        txtemai.setText(x.getStringExtra("EMAIL"));
        int e=0;
        for (int i=0; i<estados.length; i++){
            if (estados[i].equals(u.getEstado())){
                e=i;
                break;
            }
        }
        spnEstado.setSelection(e);
    }

    void Enlazar(){
        tilemail= findViewById(R.id.tilemailUsup);
        txtemai= findViewById(R.id.txtemailUsup);
        txtpassword= findViewById(R.id.txtpassUsup);
        txtpasold= findViewById(R.id.txtpassUsold);
        tilpasold= findViewById(R.id.tilpassUsold);
        tilpass= findViewById(R.id.tilpassUsup);
        btnNuevo= findViewById(R.id.btnlimpiarUsup);
        btnGrabar= findViewById(R.id.btnactualizarUs);
        btnCerrar= findViewById(R.id.btnsalirUsup);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actualizar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        spnEstado= findViewById(R.id.spnestadoUsup);
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, estados);
        spnEstado.setAdapter(adapter);
    }

    void Actualizar() {
        if (ValidarDatos()){
            ProgresoHorizontal();
        }
    }

    void Limpiar(){
        txtpassword.setText("");
        txtpasold.setText("");
        txtemai.setText("");
        spnEstado.setSelection(0);
        txtemai.requestFocus();
    }

    boolean ValidarDatos(){

        try {
            tilemail.setError("");
            tilemail.setErrorEnabled(false);
            String email = txtemai.getText().toString().trim();

            tilpass.setError("");
            tilpass.setErrorEnabled(false);
            String pass = txtpassword.getText().toString();

            tilpasold.setError("");
            tilpasold.setErrorEnabled(false);
            String passold = txtpasold.getText().toString();

            if(email.length() < 1){
                tilemail.setError("Error, la dirección de correo es obligatoria");
                tilemail.requestFocus();
                return false;
            } else if(!pCorreo.matcher(email).matches()){
                tilemail.setError("Error, el formato no coincide");
                tilemail.requestFocus();
                return false;
            }else if(instancia.dao().findByEmail2(codigo,email).size()>0){
                tilemail.setError("La dirección de correo ya existe, pruebe con otra cuenta");
                tilemail.requestFocus();
                return false;
            }

            if (!passold.equals(u.getPassword())){
                tilpasold.setError("Error, la contraseña anterior no coincide");
                tilpasold.requestFocus();
                return false;
            }else if(pass.length() < 8){
                tilpass.setError("Error, la contraseña debe contener 8 caracteres como mínimo");
                tilpass.requestFocus();
                return false;
            }

            return true;

        }catch (Exception ex){
            Toast.makeText(this, "Todos los datos son obligatorios", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    void ProgresoHorizontal(){
        ProgressDialog pd= new ProgressDialog(ActualizarUsuarioActivity.this);

        pd.setTitle("Actualizando Usuario");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_upgrade_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        u.setPassword(txtpassword.getText().toString());
                        u.setEstado(spnEstado.getSelectedItem().toString());
                        instancia.dao().update(u);
                        Toast.makeText(ActualizarUsuarioActivity.this, "El usuario: "+u.getEmail()+" se actualizó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
}