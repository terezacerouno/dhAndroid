package com.idat.dreamhouse.actualizar;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.nuevo.NuevoClienteActivity;

import java.util.regex.Pattern;

public class ActualizarClienteActivity extends AppCompatActivity {

    TextInputLayout tilNombresUpd, tilApellidosUpd, tilDniUpd;
    TextInputEditText tiedtNombresUpd, tiedtApellidosUpd, tiedtDniUpd;
    Button btnActualizar, btnLimpiarUpd;
    FloatingActionButton btnCerrarUpd;
    TextView tvCodigoCUpd;

    Pattern patron = Pattern.compile("[a-zA-Zá-úÁ-Ú ]+");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_cliente);
        getSupportActionBar().hide();
        EnlazarControles();

        Intent i = getIntent();
        Long id = i.getLongExtra("CODIGO", 0);
        Cliente cliente = ConexionDb.getInstancia(ActualizarClienteActivity.this).clienteDao().findById(id);

        tvCodigoCUpd.setText("ID: " + cliente.getId());
        tiedtNombresUpd.setText(cliente.getNombres());
        tiedtApellidosUpd.setText(cliente.getApellidos());
        tiedtDniUpd.setText(cliente.getDni());

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dni = tiedtDniUpd.getText().toString();
                Cliente clienteR = ConexionDb.getInstancia(ActualizarClienteActivity.this).clienteDao().findRepeat(dni, id);
                if(clienteR != null){
                    Toast.makeText(ActualizarClienteActivity.this, "El DNI ya se encuentra registrado", Toast.LENGTH_SHORT).show();
                } else{
                    if(ValidarDatos()){
                        ProgresoHorizontal(id);
                    }
                }
            }
        });

        btnLimpiarUpd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiedtNombresUpd.setText("");
                tiedtApellidosUpd.setText("");
                tiedtDniUpd.setText("");
            }
        });

        btnCerrarUpd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void EnlazarControles(){
        tilNombresUpd = findViewById(R.id.tilNombresCUpd);
        tilApellidosUpd = findViewById(R.id.tilApellidosCUpd);
        tilDniUpd = findViewById(R.id.tilDniCUpd);
        tiedtNombresUpd = findViewById(R.id.tiedtNombresCUpd);
        tiedtApellidosUpd = findViewById(R.id.tiedtApellidosCUpd);
        tiedtDniUpd = findViewById(R.id.tiedtDniCUpd);
        btnActualizar = findViewById(R.id.btnRegistrarCUpd);
        btnLimpiarUpd = findViewById(R.id.btnLimpiarCUpd);
        btnCerrarUpd = findViewById(R.id.btnCerrarCUpd);
        tvCodigoCUpd = findViewById(R.id.tvCodigoCUpd);
    }

    void ProgresoHorizontal(Long id){
        ProgressDialog pd= new ProgressDialog(ActualizarClienteActivity.this);

        pd.setTitle("Actualizando Cliente");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_save_alt_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Cliente clienteUpd = new Cliente(id, tiedtNombresUpd.getText().toString(),tiedtApellidosUpd.getText().toString(),tiedtDniUpd.getText().toString());
                        ConexionDb.getInstancia(ActualizarClienteActivity.this).clienteDao().update(clienteUpd);
                        Toast.makeText(ActualizarClienteActivity.this, "El cliente: "+ clienteUpd.getNombres() + " " + clienteUpd.getApellidos()
                                + " se actualizó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }

    boolean ValidarDatos(){
        tilNombresUpd.setError("");
        tilNombresUpd.setErrorEnabled(false);
        String nombres = tiedtNombresUpd.getText().toString();
        if(nombres.trim().length() < 3){
            tilNombresUpd.setError("Error, nombre muy corto");
            tiedtNombresUpd.requestFocus();
            return false;
        } else if(!patron.matcher(nombres).matches()){
            tilNombresUpd.setError("Error, solo se admiten letras");
            tiedtNombresUpd.requestFocus();
            return false;
        }

        tilApellidosUpd.setError("");
        tilApellidosUpd.setErrorEnabled(false);
        String apellidos = tiedtApellidosUpd.getText().toString();
        if(apellidos.trim().length() <= 5){
            tilApellidosUpd.setError("Error, ingrese ambos apellidos");
            tiedtApellidosUpd.requestFocus();
            return false;
        } else if(!patron.matcher(apellidos).matches()){
            tilApellidosUpd.setError("Error, solo se admiten letras");
            tiedtApellidosUpd.requestFocus();
            return false;
        }

        tilDniUpd.setError("");
        tilDniUpd.setErrorEnabled(false);
        String dni = tiedtDniUpd.getText().toString();
        if(dni.trim().length() < 8 || dni.trim().length() > 8){
            tilDniUpd.setError("Error, el DNI debe tener 8 caracteres");
            tiedtDniUpd.requestFocus();
            return false;
        }

        return true;
    }
}