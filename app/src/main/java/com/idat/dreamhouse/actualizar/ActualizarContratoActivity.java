package com.idat.dreamhouse.actualizar;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Contrato;
import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;
import com.idat.dreamhouse.nuevo.NuevoContratoActivity;

import java.util.ArrayList;
import java.util.List;

public class ActualizarContratoActivity extends AppCompatActivity {

    TextView tvCodigo;
    Spinner spnDepartamentos, spnClientes;
    TextInputLayout tilGarantia;
    TextInputEditText tiedtGarantia;
    Button btnActualizar, btnLimpiar;
    FloatingActionButton btnCerrar;
    List<Departamento> listaDepartamentos;
    List<Cliente> listaClientes;
    List<String> departamentos, clientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_contrato);
        getSupportActionBar().hide();
        EnlazarControles();

        Intent i = getIntent();
        Long id = i.getLongExtra("CODIGO", 0);
        Contrato contrato = ConexionDb.getInstancia(ActualizarContratoActivity.this).contratoDao().findById(id);
        Departamento departamento = ConexionDb.getInstancia(ActualizarContratoActivity.this).daodepartamento().getDepartamento(contrato.getCoddepa()).get(0);
        Cliente cliente = ConexionDb.getInstancia(ActualizarContratoActivity.this).clienteDao().findById(contrato.getCodcliente());

        tvCodigo.setText("ID: " + contrato.getId());

        int cont_d = 0;
        for(Departamento d: listaDepartamentos){
            if((d.getCodedificio() + "," + d.getPiso()).equals(departamento.getCodedificio() + "," + departamento.getPiso())){
                spnDepartamentos.setSelection(cont_d);
            } else{
                cont_d++;
            }
        }

        int cont_c = 0;
        for(Cliente c: listaClientes){
            if(c.getDni().equals(cliente.getDni())){
                spnClientes.setSelection(cont_c);
            } else{
                cont_c++;
            }
        }

        tiedtGarantia.setText(contrato.getGarantia().toString());

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Departamento depaRepeat = listaDepartamentos.get(spnDepartamentos.getSelectedItemPosition());
                Contrato contratoRepeat = ConexionDb.getInstancia(ActualizarContratoActivity.this).contratoDao().findRepeatUpd(depaRepeat.getId(), contrato.getId());
                if(contratoRepeat!=null){
                    Toast.makeText(ActualizarContratoActivity.this, "El departamento ya se encuentra ocupado", Toast.LENGTH_SHORT).show();
                } else{
                    ProgresoHorizontal(id);
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spnDepartamentos.setSelection(0);
                spnClientes.setSelection(0);
                tiedtGarantia.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void EnlazarControles(){
        tvCodigo = findViewById(R.id.tvCodigoCoUpd);
        tilGarantia = findViewById(R.id.tilGarantiaCoUpd);
        tiedtGarantia = findViewById(R.id.tiedtGarantiaCoUpd);
        btnActualizar = findViewById(R.id.btnActualizarCo);
        btnLimpiar = findViewById(R.id.btnLimpiarCoUpd);
        btnCerrar = findViewById(R.id.btnCerrarCoUpd);
        spnDepartamentos = findViewById(R.id.spnDepartCoUpd);
        spnClientes = findViewById(R.id.spnClienteCoUpd);

        listaDepartamentos = new ArrayList<>();
        departamentos = new ArrayList<>();

        for (Departamento d : ConexionDb.getInstancia(ActualizarContratoActivity.this).daodepartamento().listar()) {
            Edificio edi = ConexionDb.getInstancia(ActualizarContratoActivity.this).daoedificio().getEdicio(d.getCodedificio()).get(0);
            String depa = "Piso: " + d.getPiso() + "| Edificio: " + edi.getDireccion();
            departamentos.add(depa);
            listaDepartamentos.add(d);
        }
        ArrayAdapter<String> depasAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, departamentos);
        spnDepartamentos.setAdapter(depasAdapter);

        listaClientes = new ArrayList<>();
        clientes = new ArrayList<>();
        for (Cliente c : ConexionDb.getInstancia(ActualizarContratoActivity.this).clienteDao().findAll()) {
            String cli = c.getNombres() + " " + c.getApellidos();
            clientes.add(cli);
            listaClientes.add(c);
        }
        ArrayAdapter clientesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, clientes);
        spnClientes.setAdapter(clientesAdapter);
    }

    void ProgresoHorizontal(Long id) {
        ProgressDialog pd = new ProgressDialog(ActualizarContratoActivity.this);

        pd.setTitle("Actualizando Contrato");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_save_alt_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress() < pd.getMax()) {
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress() == pd.getMax()) {
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Departamento departamento = listaDepartamentos.get(spnDepartamentos.getSelectedItemPosition());
                        Cliente cliente = listaClientes.get(spnClientes.getSelectedItemPosition());
                        Double garantia = Double.parseDouble(tiedtGarantia.getText().toString());
                        Contrato contratoUpd = new Contrato(id, departamento.getId().intValue(), cliente.getId(), garantia);
                        ConexionDb.getInstancia(ActualizarContratoActivity.this).contratoDao().update(contratoUpd);
                        Toast.makeText(ActualizarContratoActivity.this, "El contrato se actualizó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
}