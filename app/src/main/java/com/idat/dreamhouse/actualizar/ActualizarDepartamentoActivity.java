package com.idat.dreamhouse.actualizar;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.TabLayoutActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;
import com.idat.dreamhouse.nuevo.NuevoDepartamentoActivity;

import java.util.ArrayList;
import java.util.List;

public class ActualizarDepartamentoActivity extends AppCompatActivity {

    Button btnGrabar, btnNuevo;
    FloatingActionButton btnCerrar;
    TextInputLayout tilpiso, tilbanos, tilarea, tilprecio, tilhabitaciones;
    TextInputEditText txtpiso, txtbanos, txtarea, txtprecio, txthabitaciones;
    Spinner spnEstado, spnEdificio;
    String [] estados={"Activo","Inactivo"};
    List<String> edificios= new ArrayList<>();
    List<Edificio> edificiosobj= new ArrayList<>();
    ConexionDb instancia;
    int codigo;
    Departamento departamento;
    Edificio edificio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_departamento);
        getSupportActionBar().hide();

        instancia= ConexionDb.getInstancia(ActualizarDepartamentoActivity.this);

        Intent x= getIntent();
        codigo= x.getIntExtra("CODIGO",0);
        departamento= instancia.daodepartamento().getDepartamento(codigo).get(0);
        edificio= instancia.daodepartamento().getEdificio(departamento.getCodedificio()).get(0);

        for (Edificio e: instancia.daoedificio().listar()
        ) {
            String nombre= "Cod: "+ e.getId()+" "+ e.getDireccion();
            edificios.add(nombre);
            edificiosobj.add(e);
        }
        Enlazar();

        txtpiso.setText(departamento.getPiso()+"");
        txtbanos.setText(departamento.getN_banos()+"");
        txtarea.setText(departamento.getArea()+"");
        txtprecio.setText(departamento.getPrecio()+"");
        txthabitaciones.setText(departamento.getN_habitaciones()+"");
        int e=0;
        for (int i=0; i<estados.length; i++){
            if (estados[i].equals(departamento.getEstado())){
                e=i;
                break;
            }
        }
        spnEstado.setSelection(e);

        int ed=0;
        for (int i=0; i<edificios.size(); i++){
            if (edificios.get(i).equals("Cod: "+ edificio.getId()+" "+ edificio.getDireccion())){
                ed=i;
                break;
            }
        }
        spnEdificio.setSelection(ed);
    }

    void Enlazar(){
        tilpiso= findViewById(R.id.tilpisoDeUp);
        tilbanos= findViewById(R.id.tilbanoDeUp);
        tilarea= findViewById(R.id.tilareaDeUp);
        tilprecio= findViewById(R.id.tilprecioDeUp);
        tilhabitaciones= findViewById(R.id.tilhabitacionesDeUp);

        txtpiso= findViewById(R.id.txtpisoDeUp);
        txtbanos= findViewById(R.id.txtbanoDeUp);
        txtarea= findViewById(R.id.txtareDeUp);
        txtprecio= findViewById(R.id.txtprecioDeUp);
        txthabitaciones= findViewById(R.id.txthabitacionesDeUp);

        btnNuevo= findViewById(R.id.btnlimpiarDeUp);
        btnGrabar= findViewById(R.id.btnactualizarDe);
        btnCerrar= findViewById(R.id.btnsalirDeUp);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Actualizar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        spnEstado= findViewById(R.id.spnestadoDeUp);
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, estados);
        spnEstado.setAdapter(adapter);

        spnEdificio=findViewById(R.id.spnedificioDeUp);
        ArrayAdapter<String> adapter1= new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, edificios);
        spnEdificio.setAdapter(adapter1);
    }

    void Limpiar(){
        txtpiso.setText("");
        txtbanos.setText("");
        txtarea.setText("");
        txtprecio.setText("");
        txthabitaciones.setText("");

        spnEstado.setSelection(0);
        spnEdificio.setSelection(0);
        txtpiso.requestFocus();
    }

    void Actualizar() {
        if (ValidarDatos()){
            ProgresoHorizontal();
        }
    }

    boolean ValidarDatos(){
        try {
            tilpiso.setError("");
            tilpiso.setErrorEnabled(false);
            String piso = txtpiso.getText().toString().trim();

            tilhabitaciones.setError("");
            tilhabitaciones.setErrorEnabled(false);
            String habitaciones = txthabitaciones.getText().toString().trim();

            tilbanos.setError("");
            tilbanos.setErrorEnabled(false);
            String banos = txtbanos.getText().toString().trim();

            tilarea.setError("");
            tilarea.setErrorEnabled(false);
            String area = txtarea.getText().toString().trim();

            tilprecio.setError("");
            tilprecio.setErrorEnabled(false);
            String precio = txtprecio.getText().toString().trim();

            if (piso.equals("")){
                tilpiso.setError("Error, la casilla piso no puede estar vacía");
                txtpiso.requestFocus();
                return false;
            }else if (Integer.parseInt(piso)<1){
                tilpiso.setError("Error, el número de piso debe ser mayor a 0");
                txtpiso.requestFocus();
                return false;
            }

            if (habitaciones.equals("")){
                tilhabitaciones.setError("Error, la casilla habitaciones no puede estar vacía");
                txthabitaciones.requestFocus();
                return false;
            }else if (Integer.parseInt(habitaciones)<1){
                tilhabitaciones.setError("Error, el número de habitaciones debe ser mayor a 0");
                txthabitaciones.requestFocus();
                return false;
            }

            if (banos.equals("")){
                tilbanos.setError("Error, la casilla baños no puede estar vacía");
                txtbanos.requestFocus();
                return false;
            }else if (Integer.parseInt(banos)<1){
                tilbanos.setError("Error, el número de baños debe ser mayor a 0");
                txtbanos.requestFocus();
                return false;
            }

            if (area.equals("")){
                tilarea.setError("Error, la casilla area no puede estar vacía");
                txtarea.requestFocus();
                return false;
            }else if (Double.parseDouble(area)<1){
                tilarea.setError("Error, el número de área debe ser mayor a 0");
                txtarea.requestFocus();
                return false;
            }

            if (precio.equals("")){
                tilprecio.setError("Error, la casilla precio no puede estar vacía");
                txtprecio.requestFocus();
                return false;
            }else if (Double.parseDouble(precio)<1){
                tilprecio.setError("Error, el número de precio debe ser mayor a 0");
                txtprecio.requestFocus();
                return false;
            }

            return true;
        }catch (Exception e){
            Toast.makeText(this, "Todos los datos son obligatorios", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    void ProgresoHorizontal(){
        ProgressDialog pd= new ProgressDialog(ActualizarDepartamentoActivity.this);

        pd.setTitle("Actualizar Departamento");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_upgrade_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Edificio edi=edificiosobj.get(spnEdificio.getSelectedItemPosition());
                        int e= edi.getId().intValue();
                        Departamento obj1= new Departamento(
                                Long.parseLong(codigo+""),
                                Integer.parseInt(txtpiso.getText().toString()),
                                Integer.parseInt(txthabitaciones.getText().toString()),
                                Integer.parseInt(txtbanos.getText().toString()),
                                Double.parseDouble(txtarea.getText().toString()),
                                Double.parseDouble(txtprecio.getText().toString()),
                                spnEstado.getSelectedItem().toString(),
                                e);
                        instancia.daodepartamento().update(obj1);
                        Toast.makeText(ActualizarDepartamentoActivity.this, "El departameto ubicado : "+edi.getDireccion()+" piso "+obj1.getPiso()+" se actualizó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }

}