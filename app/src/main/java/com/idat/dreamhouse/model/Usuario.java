package com.idat.dreamhouse.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Usuario")
public class Usuario {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "codus")
    private int id_usuario;

    @ColumnInfo(name = "emailus")
    private String email;

    @ColumnInfo(name = "passus")
    private String password;

    @ColumnInfo(name = "estus")
    private String estado;

    @ColumnInfo(name = "imgus")
    private String imagen;

    public Usuario(String email, String password, String estado) {
        this.email = email;
        this.password = password;
        this.estado = estado;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public Usuario() {
    }

    public Usuario(String email, String password, String estado, String imagen) {
        this.email = email;
        this.password = password;
        this.estado = estado;
        this.imagen = imagen;
    }

    public Usuario(int id_usuario, String email, String password, String estado, String imagen) {
        this.id_usuario = id_usuario;
        this.email = email;
        this.password = password;
        this.estado = estado;
        this.imagen = imagen;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
