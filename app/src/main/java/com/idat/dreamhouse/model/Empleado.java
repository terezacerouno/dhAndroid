package com.idat.dreamhouse.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Empleado")
public class Empleado {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "codemple")
    private int id_empleado;

    @ColumnInfo(name = "nomemple")
    private String nombres;

    @ColumnInfo(name = "apemple")
    private String a_paterno;

    @ColumnInfo(name = "amemple")
    private String a_materno;

    @ColumnInfo(name = "dniemple")
    private String dni;

    @ColumnInfo(name = "telemple")
    private String telefono;

    @ColumnInfo(name = "estemple")
    private String estado;

    public Empleado(int id_empleado, String nombres, String a_paterno, String a_materno, String dni, String telefono, String estado) {
        this.id_empleado = id_empleado;
        this.nombres = nombres;
        this.a_paterno = a_paterno;
        this.a_materno = a_materno;
        this.dni = dni;
        this.telefono = telefono;
        this.estado = estado;
    }

    public Empleado() {
    }

    public Empleado(String nombres, String a_paterno, String a_materno, String dni, String telefono, String estado) {
        this.nombres = nombres;
        this.a_paterno = a_paterno;
        this.a_materno = a_materno;
        this.dni = dni;
        this.telefono = telefono;
        this.estado = estado;
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getA_paterno() {
        return a_paterno;
    }

    public void setA_paterno(String a_paterno) {
        this.a_paterno = a_paterno;
    }

    public String getA_materno() {
        return a_materno;
    }

    public void setA_materno(String a_materno) {
        this.a_materno = a_materno;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
