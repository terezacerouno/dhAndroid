package com.idat.dreamhouse.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Cliente")
public class Cliente {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "codcliente")
    private Long id;

    @ColumnInfo(name = "nomcliente")
    private String nombres;

    @ColumnInfo(name = "apecliente")
    private String apellidos;

    @ColumnInfo(name = "dnicliente")
    private String dni;

    public Cliente() {
    }

    public Cliente(Long id, String nombres, String apellidos, String dni) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.dni = dni;
    }

    public Cliente(String nombres, String apellidos, String dni) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.dni = dni;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
}
