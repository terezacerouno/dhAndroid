package com.idat.dreamhouse.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "Departamento", foreignKeys ={
        @ForeignKey(entity = Edificio.class,
                parentColumns = "codedificio",
                childColumns = "codedificio",
                onDelete = ForeignKey.CASCADE)
})
public class Departamento {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "coddepa")
    private Long id;

    @ColumnInfo(name = "pisodepa")
    private Integer piso;

    @ColumnInfo(name = "habitacionesdepa")
    private Integer n_habitaciones;

    @ColumnInfo(name = "banosdepa")
    private Integer n_banos;

    @ColumnInfo(name = "areadepa")
    private Double area;

    @ColumnInfo(name = "preciodepa")
    private Double precio;

    @ColumnInfo(name = "estadepa")
    private String estado;

    @ColumnInfo(name = "codedificio")
    private int codedificio;

    public Departamento(Long id, Integer piso, Integer n_habitaciones, Integer n_banos, Double area, Double precio, String estado, int codedificio) {
        this.id = id;
        this.piso = piso;
        this.n_habitaciones = n_habitaciones;
        this.n_banos = n_banos;
        this.area = area;
        this.precio = precio;
        this.estado = estado;
        this.codedificio = codedificio;
    }

    public Departamento() {
    }

    public Departamento(Integer piso, Integer n_habitaciones, Integer n_banos, Double area, Double precio, String estado, int codedificio) {
        this.piso = piso;
        this.n_habitaciones = n_habitaciones;
        this.n_banos = n_banos;
        this.area = area;
        this.precio = precio;
        this.estado = estado;
        this.codedificio = codedificio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    public Integer getN_habitaciones() {
        return n_habitaciones;
    }

    public void setN_habitaciones(Integer n_habitaciones) {
        this.n_habitaciones = n_habitaciones;
    }

    public Integer getN_banos() {
        return n_banos;
    }

    public void setN_banos(Integer n_banos) {
        this.n_banos = n_banos;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getCodedificio() {
        return codedificio;
    }

    public void setCodedificio(int codedificio) {
        this.codedificio = codedificio;
    }
}
