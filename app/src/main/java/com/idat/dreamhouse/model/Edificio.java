package com.idat.dreamhouse.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Edificio")
public class Edificio {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "codedificio")
    private Long id;

    @ColumnInfo(name = "pisosedificio")
    private Integer n_pisos;

    @ColumnInfo(name = "diredificio")
    private String direccion;

    @ColumnInfo(name = "desedificio")
    private String descripcion;

    @ColumnInfo(name = "imgedificio")
    private String imagen;

    public Edificio(Integer n_pisos, String direccion, String descripcion) {
        this.n_pisos = n_pisos;
        this.direccion = direccion;
        this.descripcion = descripcion;
    }

    public Edificio() {
    }

    public Edificio(Integer n_pisos, String direccion, String descripcion, String imagen) {
        this.n_pisos = n_pisos;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public Edificio(Long id, Integer n_pisos, String direccion, String descripcion, String imagen) {
        this.id = id;
        this.n_pisos = n_pisos;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getN_pisos() {
        return n_pisos;
    }

    public void setN_pisos(Integer n_pisos) {
        this.n_pisos = n_pisos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
