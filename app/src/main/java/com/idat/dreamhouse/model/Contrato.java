package com.idat.dreamhouse.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;


@Entity(tableName = "Contrato", foreignKeys = {
        @ForeignKey(entity = Departamento.class,
                parentColumns = "coddepa",
                childColumns = "coddepa",
                onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = Cliente.class,
                parentColumns = "codcliente",
                childColumns = "codcliente",
                onDelete = ForeignKey.CASCADE)
})
public class Contrato {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "codcont")
    private Long id;

    @ColumnInfo(name = "coddepa")
    private int coddepa;

    @ColumnInfo(name = "codcliente")
    private Long codcliente;

    @ColumnInfo(name = "garantia")
    private Double garantia;

    public Contrato() {
    }

    public Contrato(Long id, int coddepa, Long codcliente, Double garantia) {
        this.id = id;
        this.coddepa = coddepa;
        this.codcliente = codcliente;
        this.garantia = garantia;
    }

    public Contrato(int coddepa, Long codcliente, Double garantia) {
        this.coddepa = coddepa;
        this.codcliente = codcliente;
        this.garantia = garantia;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCoddepa() {
        return coddepa;
    }

    public void setCoddepa(int coddepa) {
        this.coddepa = coddepa;
    }

    public Long getCodcliente() {
        return codcliente;
    }

    public void setCodcliente(Long codcliente) {
        this.codcliente = codcliente;
    }

    public Double getGarantia() {
        return garantia;
    }

    public void setGarantia(Double garantia) {
        this.garantia = garantia;
    }
}
