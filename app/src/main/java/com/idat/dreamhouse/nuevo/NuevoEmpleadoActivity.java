package com.idat.dreamhouse.nuevo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.TabLayoutActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;

import java.util.regex.Pattern;

public class NuevoEmpleadoActivity extends AppCompatActivity {

    Button btnGrabar, btnNuevo;
    FloatingActionButton btnCerrar;
    TextInputLayout tilnombre, tilap, tilam, tildni, tiltel;
    TextInputEditText txtnombre, txtap, txtam, txtdni, txttel;
    Spinner spnEstado;
    String [] estados={"Activo","Inactivo"};
    ConexionDb instancia;
    Pattern patron = Pattern.compile("[a-zA-Zá-úÁ-Ú ]+");
    Pattern pCelular= Pattern.compile("[9][0-9]{8}");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_nuevo_empleado);
        instancia= ConexionDb.getInstancia(NuevoEmpleadoActivity.this);
        Enlazar();
    }

    void Enlazar(){
        tilnombre= findViewById(R.id.tilnombreEm);
        tilap= findViewById(R.id.tilapEm);
        tilam= findViewById(R.id.tilamEm);
        tildni= findViewById(R.id.tildniEm);
        tiltel= findViewById(R.id.tiltelEm);

        txtnombre=findViewById(R.id.txtnombreEm);
        txtap=findViewById(R.id.txtapEm);
        txtam=findViewById(R.id.txtamEm);
        txtdni=findViewById(R.id.txtdniEm);
        txttel=findViewById(R.id.txttelEm);

        btnNuevo= findViewById(R.id.btnlimpiarEm);
        btnGrabar= findViewById(R.id.btnnuevoEm);
        btnCerrar= findViewById(R.id.btnsalirEm);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Agregar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        spnEstado= findViewById(R.id.spnestadoEm);
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, estados);
        spnEstado.setAdapter(adapter);
    }

    void Limpiar(){
        txtnombre.setText("");
        txtap.setText("");
        txtam.setText("");
        txtdni.setText("");
        txttel.setText("");
        spnEstado.setSelection(0);
        txtnombre.requestFocus();
    }

    void Agregar() {
        if (ValidarDatos()){
            ProgresoHorizontal();
        }
    }

    boolean ValidarDatos(){
        try {
            tilnombre.setError("");
            tilnombre.setErrorEnabled(false);
            String nombres = txtnombre.getText().toString();

            tilap.setError("");
            tilap.setErrorEnabled(false);
            String apellidos = txtap.getText().toString();

            tilam.setError("");
            tilam.setErrorEnabled(false);
            String apellidosm = txtam.getText().toString();

            tildni.setError("");
            tildni.setErrorEnabled(false);
            String dni = txtdni.getText().toString();

            tiltel.setError("");
            tiltel.setErrorEnabled(false);
            String telefono = txttel.getText().toString();

            if(nombres.trim().length() < 3){
                tilnombre.setError("Error, nombre muy corto");
                txtnombre.requestFocus();
                return false;
            } else if(!patron.matcher(nombres).matches()){
                tilnombre.setError("Error, solo se admiten letras");
                tilnombre.requestFocus();
                return false;
            }

            if(apellidos.trim().length() < 3){
                tilap.setError("Error, apellido muy corto");
                tilap.requestFocus();
                return false;
            } else if(!patron.matcher(apellidos).matches()){
                tilap.setError("Error, solo se admiten letras");
                tilap.requestFocus();
                return false;
            }

            if(apellidosm.trim().length() < 3){
                tilam.setError("Error, apellido muy corto");
                tilam.requestFocus();
                return false;
            } else if(!patron.matcher(apellidosm).matches()){
                tilam.setError("Error, solo se admiten letras");
                tilam.requestFocus();
                return false;
            }

            if(dni.trim().length() < 8 || dni.trim().length() > 8){
                tildni.setError("Error, el DNI debe tener 8 caracteres");
                tildni.requestFocus();
                return false;
            }else if(instancia.daoempleado().findByDni(Integer.parseInt(txtdni.getText().toString())).size()>0){
                tildni.setError("El dni ingresado ya existe");
                tildni.requestFocus();
                return false;
            }

            if(telefono.trim().length() < 9 || telefono.trim().length() > 9){
                tiltel.setError("Error, el teléfono debe tener 9 caracteres");
                tiltel.requestFocus();
                return false;
            }else if(!pCelular.matcher(telefono).matches()){
                tiltel.setError("Error, el formato del teléfono no coincide");
                tiltel.requestFocus();
                return false;
            }

            return true;

        }catch (Exception e){
            Toast.makeText(this, "Todos los datos son obligatorios", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    void ProgresoHorizontal(){
        ProgressDialog pd= new ProgressDialog(NuevoEmpleadoActivity.this);

        pd.setTitle("Grabando Empleado");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_save_alt_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Empleado obj1= new Empleado(txtnombre.getText().toString().trim(),txtap.getText().toString().trim(),txtam.getText().toString().trim(),txtdni.getText().toString(),txttel.getText().toString(),spnEstado.getSelectedItem().toString());
                        instancia.daoempleado().create(obj1);
                        Toast.makeText(NuevoEmpleadoActivity.this, "El empleado: "+obj1.getNombres()+" se guardó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
}