package com.idat.dreamhouse.nuevo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.TabLayoutActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.extras.Extras;
import com.idat.dreamhouse.model.Usuario;

import java.util.regex.Pattern;

public class NuevoUsuarioActivity extends AppCompatActivity {

    Button btnGrabar, btnNuevo;
    FloatingActionButton btnCerrar;
    TextInputLayout tilemail, tilpass;
    TextInputEditText txtemai, txtpassword;
    Spinner spnEstado;
    String [] estados={"Activo","Inactivo"};
    ConexionDb instancia;
    Pattern pCorreo= Patterns.EMAIL_ADDRESS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);
        getSupportActionBar().hide();

        instancia= ConexionDb.getInstancia(NuevoUsuarioActivity.this);
        Enlazar();
    }

    void Enlazar(){
        tilemail= findViewById(R.id.tilemailUs);
        txtemai= findViewById(R.id.txtemailUs);
        txtpassword= findViewById(R.id.txtpassUs);
        tilpass= findViewById(R.id.tilpassUs);
        btnNuevo= findViewById(R.id.btnlimpiarUs);
        btnGrabar= findViewById(R.id.btnnuevoUs);
        btnCerrar= findViewById(R.id.btnsalirUs);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Agregar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        spnEstado= findViewById(R.id.spnestadoUs);
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, estados);
        spnEstado.setAdapter(adapter);
    }

    void Agregar() {
        if (ValidarDatos()){
            ProgresoHorizontal();
        }
    }

    boolean ValidarDatos(){

        try {
            tilemail.setError("");
            tilemail.setErrorEnabled(false);
            String email = txtemai.getText().toString().trim();

            tilpass.setError("");
            tilpass.setErrorEnabled(false);
            String pass = txtpassword.getText().toString();

            if(email.length() < 1){
                tilemail.setError("Error, la dirección de correo es obligatoria");
                tilemail.requestFocus();
                return false;
            } else if(!pCorreo.matcher(email).matches()){
                tilemail.setError("Error, el formato no coincide");
                tilemail.requestFocus();
                return false;
            }else if(instancia.dao().findByEmail(email).size()>0){
                tilemail.setError("La dirección de correo ya existe, pruebe con otra cuenta");
                tilemail.requestFocus();
                return false;
            }

            if(pass.length() < 8){
                tilpass.setError("Error, la contraseña debe contener 8 caracteres como mínimo");
                tilpass.requestFocus();
                return false;
            }

            return true;

        }catch (Exception ex){
            Toast.makeText(this, "Todos los datos son obligatorios", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    void Limpiar(){
        txtpassword.setText("");
        txtemai.setText("");
        spnEstado.setSelection(0);
        txtemai.requestFocus();
    }

    void ProgresoHorizontal(){
        ProgressDialog pd= new ProgressDialog(NuevoUsuarioActivity.this);

        pd.setTitle("Grabando Usuario");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_save_alt_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Usuario obj1= new Usuario(txtemai.getText().toString().trim(),txtpassword.getText().toString(),spnEstado.getSelectedItem().toString());
                        instancia.dao().create(obj1);
                        Usuario obj= instancia.dao().findByEmail(txtemai.getText().toString()).get(0);
                        obj.setImagen("u"+obj.getId_usuario());
                        instancia.dao().update(obj);
                        Toast.makeText(NuevoUsuarioActivity.this, "El usuario: "+obj1.getEmail()+" se guardó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
}