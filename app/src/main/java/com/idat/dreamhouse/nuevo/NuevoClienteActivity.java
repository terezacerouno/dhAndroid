package com.idat.dreamhouse.nuevo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.TabLayoutActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Empleado;

import java.util.regex.Pattern;

public class NuevoClienteActivity extends AppCompatActivity {

    TextInputLayout tilNombres, tilApellidos, tilDni;
    TextInputEditText tiedtNombres, tiedtApellidos, tiedtDni;
    Button btnRegistrar, btnLimpiar;
    FloatingActionButton btnCerrar;

    Pattern patron = Pattern.compile("[a-zA-Zá-úÁ-Ú ]+");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_cliente);
        getSupportActionBar().hide();
        EnlazarControles();

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dni = tiedtDni.getText().toString();
                Cliente clienteRep = ConexionDb.getInstancia(NuevoClienteActivity.this).clienteDao().findByDni(dni);
                if(clienteRep != null){
                    Toast.makeText(NuevoClienteActivity.this, "El DNI ya se encuentra registrado", Toast.LENGTH_SHORT).show();
                } else{
                    if(ValidarDatos()){
                        ProgresoHorizontal();    
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiedtNombres.setText("");
                tiedtApellidos.setText("");
                tiedtDni.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void EnlazarControles(){
        tilNombres = findViewById(R.id.tilNombresC);
        tilApellidos = findViewById(R.id.tilApellidosC);
        tilDni = findViewById(R.id.tilDniC);
        tiedtNombres = findViewById(R.id.tiedtNombresC);
        tiedtApellidos = findViewById(R.id.tiedtApellidosC);
        tiedtDni = findViewById(R.id.tiedtDniC);
        btnRegistrar = findViewById(R.id.btnRegistrarC);
        btnLimpiar = findViewById(R.id.btnLimpiarC);
        btnCerrar = findViewById(R.id.btnCerrarC);
    }

    void ProgresoHorizontal(){
        ProgressDialog pd= new ProgressDialog(NuevoClienteActivity.this);

        pd.setTitle("Guardando Cliente");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_save_alt_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Cliente cliente= new Cliente(tiedtNombres.getText().toString(),tiedtApellidos.getText().toString(),tiedtDni.getText().toString());
                        ConexionDb.getInstancia(NuevoClienteActivity.this).clienteDao().create(cliente);
                        Toast.makeText(NuevoClienteActivity.this, "El cliente: "+ cliente.getNombres() + " " + cliente.getApellidos()
                                + " se guardó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
    
    boolean ValidarDatos(){
        tilNombres.setError("");
        tilNombres.setErrorEnabled(false);
        String nombres = tiedtNombres.getText().toString();
        if(nombres.trim().length() < 3){
            tilNombres.setError("Error, nombre muy corto");
            tiedtNombres.requestFocus();
            return false;
        } else if(!patron.matcher(nombres).matches()){
            tilNombres.setError("Error, solo se admiten letras");
            tiedtNombres.requestFocus();
            return false;
        }

        tilApellidos.setError("");
        tilApellidos.setErrorEnabled(false);
        String apellidos = tiedtApellidos.getText().toString();
        if(apellidos.trim().length() <= 5){
            tilApellidos.setError("Error, ingrese ambos apellidos");
            tiedtApellidos.requestFocus();
            return false;
        } else if(!patron.matcher(apellidos).matches()){
            tilApellidos.setError("Error, solo se admiten letras");
            tiedtApellidos.requestFocus();
            return false;
        }

        tilDni.setError("");
        tilDni.setErrorEnabled(false);
        String dni = tiedtDni.getText().toString();
        if(dni.trim().length() < 8 || dni.trim().length() > 8){
            tilDni.setError("Error, el DNI debe tener 8 caracteres");
            tiedtDni.requestFocus();
            return false;
        }
        
        return true;
    }
}