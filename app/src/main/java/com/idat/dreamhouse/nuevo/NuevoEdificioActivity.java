package com.idat.dreamhouse.nuevo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.TabLayoutActivity;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Edificio;
import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;

public class NuevoEdificioActivity extends AppCompatActivity {


    Button btnGrabar, btnNuevo;
    FloatingActionButton btnCerrar;
    TextInputLayout tildesc, tildire, tilnp;
    TextInputEditText txtdesc, txtdire, txtnp;
    ConexionDb instancia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_edificio);
        getSupportActionBar().hide();

        instancia= ConexionDb.getInstancia(NuevoEdificioActivity.this);
        Enlazar();
    }

    void Enlazar(){
        tildesc= findViewById(R.id.tildescEd);
        tildire= findViewById(R.id.tildireccionEd);
        tilnp= findViewById(R.id.tilpisosEd);
        txtdesc= findViewById(R.id.txtdescEd);
        txtdire= findViewById(R.id.txtdireccionEd);
        txtnp= findViewById(R.id.txtpisosEd);


        btnNuevo= findViewById(R.id.btnlimpiarEd);
        btnGrabar= findViewById(R.id.btnnuevoEd);
        btnCerrar= findViewById(R.id.btnsalirEd);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Agregar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void Limpiar(){
        txtdesc.setText("");
        txtdire.setText("");
        txtnp.setText("");
        txtdire.requestFocus();
    }

    void Agregar() {
        if (ValidarDatos()){
            ProgresoHorizontal();
        }
    }

    boolean ValidarDatos(){
        try {
            tildire.setError("");
            tildire.setErrorEnabled(false);
            String direccion = txtdire.getText().toString().trim();

            tildesc.setError("");
            tildesc.setErrorEnabled(false);
            String descripcion = txtdesc.getText().toString().trim();

            tilnp.setError("");
            tilnp.setErrorEnabled(false);

            if (direccion.equals("")){
                tildire.setError("Error, la dirección no puede estar vacía");
                txtdire.requestFocus();
                return false;
            }

            if (descripcion.equals("")){
                tildesc.setError("Error, la descripción no puede estar vacía");
                txtdesc.requestFocus();
                return false;
            }

            if (txtnp.getText().toString().equals("")){
                tilnp.setError("Error, el número de pisos no puede estar vacía");
                txtnp.requestFocus();
                return false;
            }else if (Integer.parseInt(txtnp.getText().toString())<1){
                tilnp.setError("Error, el número de pisos debe ser mayor a 0");
                txtnp.requestFocus();
                return false;
            }

            return true;
        }catch (Exception e){
            Toast.makeText(this, "Todos los datos son obligatorios", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    void ProgresoHorizontal(){
        ProgressDialog pd= new ProgressDialog(NuevoEdificioActivity.this);

        pd.setTitle("Grabando Edificio");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_save_alt_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress()<pd.getMax()){
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress()==pd.getMax()){
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Long cod;
                        Edificio obj1= new Edificio(Integer.parseInt(txtnp.getText().toString()),txtdire.getText().toString().trim(),txtdesc.getText().toString().trim());
                        instancia.daoedificio().create(obj1);
                        cod=instancia.daoedificio().Obtenerultimocodigo().get(0).getId();
                        obj1.setId(cod);
                        obj1.setImagen("e"+cod);
                        instancia.daoedificio().update(obj1);
                        Toast.makeText(NuevoEdificioActivity.this, "El edificio en: "+obj1.getDireccion()+" se guardó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
}