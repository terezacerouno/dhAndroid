package com.idat.dreamhouse.nuevo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Contrato;
import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;

import java.util.ArrayList;
import java.util.List;

public class NuevoContratoActivity extends AppCompatActivity {

    Spinner spnDepartamentos, spnClientes;
    TextInputLayout tilGarantia;
    TextInputEditText tiedtGarantia;
    Button btnGuardar, btnLimpiar;
    FloatingActionButton btnCerrar;
    List<Departamento> listaDepartamentos;
    List<Cliente> listaClientes;
    List<String> departamentos, clientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_contrato);
        getSupportActionBar().hide();
        EnlazarControles();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Departamento depaRepeat = listaDepartamentos.get(spnDepartamentos.getSelectedItemPosition());
                Contrato contratoRepeat = ConexionDb.getInstancia(NuevoContratoActivity.this).contratoDao().findRepeat(depaRepeat.getId());
                if(contratoRepeat != null){
                    Toast.makeText(NuevoContratoActivity.this, "El departamento ya se encuentra ocupado", Toast.LENGTH_SHORT).show();
                } else{
                    ProgresoHorizontal();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiedtGarantia.setText("");
                spnDepartamentos.setSelection(0);
                spnClientes.setSelection(0);
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void EnlazarControles() {
        tilGarantia = findViewById(R.id.tilGarantiaCo);
        tiedtGarantia = findViewById(R.id.tiedtGarantiaCo);
        btnGuardar = findViewById(R.id.btnRegistrarCo);
        btnLimpiar = findViewById(R.id.btnLimpiarCo);
        btnCerrar = findViewById(R.id.btnCerrarCo);
        spnClientes = findViewById(R.id.spnClienteCo);
        spnDepartamentos = findViewById(R.id.spnDepartCo);

        listaDepartamentos = new ArrayList<>();
        departamentos = new ArrayList<>();

        for (Departamento d : ConexionDb.getInstancia(NuevoContratoActivity.this).daodepartamento().listar()) {
            Edificio edi = ConexionDb.getInstancia(NuevoContratoActivity.this).daoedificio().getEdicio(d.getCodedificio()).get(0);
            String depa = "Piso: " + d.getPiso() + "| Edificio: " + edi.getDireccion();
            departamentos.add(depa);
            listaDepartamentos.add(d);
        }
        ArrayAdapter<String> depasAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, departamentos);
        spnDepartamentos.setAdapter(depasAdapter);

        listaClientes = new ArrayList<>();
        clientes = new ArrayList<>();
        for (Cliente c : ConexionDb.getInstancia(NuevoContratoActivity.this).clienteDao().findAll()) {
            String cli = c.getNombres() + " " + c.getApellidos();
            clientes.add(cli);
            listaClientes.add(c);
        }
        ArrayAdapter clientesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, clientes);
        spnClientes.setAdapter(clientesAdapter);
    }

    void ProgresoHorizontal() {
        ProgressDialog pd = new ProgressDialog(NuevoContratoActivity.this);

        pd.setTitle("Grabando Contrato");
        pd.setMessage("Espere un momento...");
        pd.setIcon(R.drawable.ic_baseline_save_alt_24);
        pd.setProgressStyle(1);
        pd.setCancelable(false);
        pd.setProgress(0);
        pd.setMax(10);
        pd.show();

        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pd.getProgress() < pd.getMax()) {
                    try {
                        Thread.sleep(1000);
                        pd.incrementProgressBy(2);
                        if (pd.getProgress() == pd.getMax()) {
                            pd.dismiss();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Departamento departamento = listaDepartamentos.get(spnDepartamentos.getSelectedItemPosition());
                        Cliente cliente = listaClientes.get(spnClientes.getSelectedItemPosition());
                        Double garantia = Double.parseDouble(tiedtGarantia.getText().toString());
                        Contrato contrato = new Contrato(departamento.getId().intValue(), cliente.getId(), garantia);
                        ConexionDb.getInstancia(NuevoContratoActivity.this).contratoDao().create(contrato);
                        Toast.makeText(NuevoContratoActivity.this, "El contrato se guardó correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        hilo.start();
    }
}