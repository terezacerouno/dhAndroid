package com.idat.dreamhouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.actualizar.ActualizarContratoActivity;
import com.idat.dreamhouse.adapter.ClienteAdapter;
import com.idat.dreamhouse.adapter.ContratoAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Contrato;
import com.idat.dreamhouse.nuevo.NuevoContratoActivity;

import java.util.List;

public class FragmentContratos extends Fragment {

    Button btnNuevo;
    ListView lvContratos;
    ContratoAdapter adapter;
    List<Contrato> listaContratos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_contratos, container, false);
        EnlazarControles(vista);
        CargarDatos();

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NuevoContratoActivity.class);
                startActivity(i);
            }
        });

        lvContratos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contrato contrato = listaContratos.get(position);
                Intent i = new Intent(getActivity(), ActualizarContratoActivity.class);
                i.putExtra("CODIGO", contrato.getId());
                startActivity(i);
            }
        });

        lvContratos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Contrato contrato = listaContratos.get(position);
                AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());

                dialog.setTitle("Eliminar Contrato");
                dialog.setIcon(R.drawable.ic_baseline_delete_outline_24);
                dialog.setCancelable(false);
                dialog.setMessage("¿Está seguro de eliminar el contrato?");

                dialog.setPositiveButton("SÍ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConexionDb.getInstancia(getContext()).contratoDao().deleteById(contrato.getId());
                        Toast.makeText(getActivity(), "Contrato eliminado correctamente", Toast.LENGTH_SHORT).show();
                        CargarDatos();
                    }
                });
                dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Se canceló la operación", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.create().show();

                return true;
            }
        });

        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        CargarDatos();
    }

    void EnlazarControles(View vista){
        btnNuevo = vista.findViewById(R.id.btnNuevoCo);
        lvContratos = vista.findViewById(R.id.lvContratos);
    }

    void CargarDatos(){
        listaContratos = ConexionDb.getInstancia(getContext()).contratoDao().findAll();
        adapter = new ContratoAdapter(getContext(), R.layout.fila_contratos, listaContratos);
        lvContratos.setAdapter(adapter);
    }
}