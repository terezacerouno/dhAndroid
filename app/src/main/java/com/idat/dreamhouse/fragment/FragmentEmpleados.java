package com.idat.dreamhouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.actualizar.ActualizarEmpleadoActivity;
import com.idat.dreamhouse.actualizar.ActualizarUsuarioActivity;
import com.idat.dreamhouse.adapter.EmpleadoAdapter;
import com.idat.dreamhouse.adapter.UsuarioAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;
import com.idat.dreamhouse.nuevo.NuevoEmpleadoActivity;
import com.idat.dreamhouse.nuevo.NuevoUsuarioActivity;

import java.util.List;

public class FragmentEmpleados extends Fragment {

    ListView lvPesonalizado;
    EmpleadoAdapter adapter;
    List<Empleado> empleados;
    Button btnNuevo;
    ConexionDb instancia;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_empleados, container, false);
        instancia= ConexionDb.getInstancia(getContext());
        Enlazar(vista);
        TraerEmpleados();
        return vista;
    }
    void Enlazar(View v){
        lvPesonalizado= v.findViewById(R.id.lvEmpleados);
        btnNuevo= v.findViewById(R.id.btnNuevoEmpleado);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NuevoEmpleadoActivity.class);
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Empleado e= empleados.get(position);
                Intent i = new Intent(getActivity(),
                        ActualizarEmpleadoActivity.class);
                i.putExtra("CODIGO", e.getId_empleado());
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Empleado e= empleados.get(position);

                AlertDialog.Builder d= new AlertDialog.Builder(getActivity());

                d.setTitle("Eliminar Empleado");
                d.setIcon(R.drawable.ic_baseline_delete_outline_24);
                d.setCancelable(false);
                d.setMessage("¿Está seguro de eliminar el empleado "+e.getNombres()+"?");

                d.setPositiveButton("SÍ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        instancia.daoempleado().deletecod(e.getId_empleado());
                        Toast.makeText(getActivity(), "El empleado "+ e.getNombres()+" ha sido eliminado", Toast.LENGTH_SHORT).show();
                        TraerEmpleados();
                    }
                });
                d.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Se canceló la operación", Toast.LENGTH_SHORT).show();
                    }
                });
                d.create().show();
                return  true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        TraerEmpleados();
    }

    public void TraerEmpleados(){
        empleados= instancia.daoempleado().listar();
        adapter = new EmpleadoAdapter(getContext(), R.layout.fila_empleados, empleados);
        lvPesonalizado.setAdapter(adapter);
    }
}