package com.idat.dreamhouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.actualizar.ActualizarEdificioActivity;
import com.idat.dreamhouse.actualizar.ActualizarUsuarioActivity;
import com.idat.dreamhouse.adapter.EdificioAdapter;
import com.idat.dreamhouse.adapter.EmpleadoAdapter;
import com.idat.dreamhouse.adapter.UsuarioAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Edificio;
import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;
import com.idat.dreamhouse.nuevo.NuevoEdificioActivity;
import com.idat.dreamhouse.nuevo.NuevoUsuarioActivity;

import java.util.List;

public class FragmentEdificios extends Fragment {

    ListView lvPesonalizado;
    EdificioAdapter adapter;
    List<Edificio> edificios;
    Button btnNuevo;
    ConexionDb instancia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista= inflater.inflate(R.layout.fragment_edificios, container, false);
        instancia= ConexionDb.getInstancia(getContext());
        Enlazar(vista);
        TraerEdificios();
        return vista;
    }

    void Enlazar(View v){
        lvPesonalizado= v.findViewById(R.id.lvEdificios);
        btnNuevo= v.findViewById(R.id.btnNuevoEdificio);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NuevoEdificioActivity.class);
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Edificio e= edificios.get(position);
                Intent i = new Intent(getActivity(),
                        ActualizarEdificioActivity.class);
                i.putExtra("CODIGO", e.getId().intValue());
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Edificio e= edificios.get(position);

                AlertDialog.Builder d= new AlertDialog.Builder(getActivity());

                d.setTitle("Eliminar Edificio");
                d.setIcon(R.drawable.ic_baseline_delete_outline_24);
                d.setCancelable(false);
                d.setMessage("¿Está seguro de eliminar el edificio "+e.getId()+"?");

                d.setPositiveButton("SÍ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        instancia.daoedificio().deletecod(e.getId().intValue());
                        Toast.makeText(getActivity(), "El edificio "+ e.getId()+" ha sido eliminado", Toast.LENGTH_SHORT).show();
                        TraerEdificios();
                    }
                });
                d.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Se canceló la operación", Toast.LENGTH_SHORT).show();
                    }
                });
                d.create().show();
                return  true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        TraerEdificios();
    }

    public void TraerEdificios(){
        edificios= instancia.daoedificio().listar();
        adapter = new EdificioAdapter(getContext(), R.layout.fila_edificios, edificios);
        lvPesonalizado.setAdapter(adapter);
    }
}