package com.idat.dreamhouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.idat.dreamhouse.nuevo.NuevoUsuarioActivity;
import com.idat.dreamhouse.R;
import com.idat.dreamhouse.actualizar.ActualizarUsuarioActivity;
import com.idat.dreamhouse.adapter.UsuarioAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Usuario;

import java.util.List;

public class FragmentUsuarios extends Fragment {
    ListView lvPesonalizado;
    UsuarioAdapter adapter;
    List<Usuario> usuarios;
    Button btnNuevo;
    ConexionDb instancia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista=inflater.inflate(R.layout.fragment_usuarios, container, false);
        instancia= ConexionDb.getInstancia(getContext());
        Enlazar(vista);
        TraerUsuarios();
        return vista;
    }

    void Enlazar(View v){
        lvPesonalizado= v.findViewById(R.id.lvUsuarios);
        btnNuevo= v.findViewById(R.id.btnNuevoUsuario);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NuevoUsuarioActivity.class);
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Usuario u= usuarios.get(position);
                Intent i = new Intent(getActivity(),
                        ActualizarUsuarioActivity.class);
                i.putExtra("CODIGO", u.getId_usuario());
                i.putExtra("EMAIL", u.getEmail());
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Usuario u= usuarios.get(position);

                AlertDialog.Builder d= new AlertDialog.Builder(getActivity());

                d.setTitle("Eliminar Usuario");
                d.setIcon(R.drawable.ic_baseline_delete_outline_24);
                d.setCancelable(false);
                d.setMessage("¿Está seguro de eliminar el usuario "+u.getEmail()+"?");

                d.setPositiveButton("SÍ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        instancia.dao().deletecod(u.getId_usuario());
                        Toast.makeText(getActivity(), "El usuario "+ u.getEmail()+" ha sido eliminado", Toast.LENGTH_SHORT).show();
                        TraerUsuarios();
                    }
                });
                d.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Se canceló la operación", Toast.LENGTH_SHORT).show();
                    }
                });
                d.create().show();
                return  true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        TraerUsuarios();
    }

    public void TraerUsuarios(){
        usuarios= instancia.dao().listar();
        adapter = new UsuarioAdapter(getContext(), R.layout.fila_usuarios, usuarios);
        lvPesonalizado.setAdapter(adapter);
    }
}