package com.idat.dreamhouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.actualizar.ActualizarClienteActivity;
import com.idat.dreamhouse.adapter.ClienteAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.nuevo.NuevoClienteActivity;
import com.idat.dreamhouse.nuevo.NuevoEmpleadoActivity;

import java.util.List;

public class FragmentClientes extends Fragment {

    Button btnNuevo;
    ListView lvClientes;
    ClienteAdapter adapter;
    List<Cliente> listaClientes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_clientes, container, false);
        EnlazarControles(vista);
        cargarDatos();

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NuevoClienteActivity.class);
                startActivity(i);
            }
        });

        lvClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cliente cliente = listaClientes.get(position);
                Intent i = new Intent(getActivity(), ActualizarClienteActivity.class);
                i.putExtra("CODIGO", cliente.getId());
                startActivity(i);
            }
        });

        lvClientes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cliente cliente = listaClientes.get(position);

                AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());

                dialog.setTitle("Eliminar Cliente");
                dialog.setIcon(R.drawable.ic_baseline_delete_outline_24);
                dialog.setCancelable(false);
                dialog.setMessage("¿Está seguro de eliminar el cliente "+ cliente.getNombres() + " " + cliente.getApellidos() + "?");

                dialog.setPositiveButton("SÍ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConexionDb.getInstancia(getContext()).clienteDao().deleteById(cliente.getId());
                        Toast.makeText(getActivity(), "Cliente "+ cliente.getNombres() + " " + cliente.getApellidos() +
                                " eliminado correctamente", Toast.LENGTH_SHORT).show();
                        cargarDatos();
                    }
                });
                dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Se canceló la operación", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.create().show();

                return true;
            }
        });

        return vista;
    }

    @Override
    public void onResume() {
        super.onResume();
        cargarDatos();
    }

    void EnlazarControles(View vista){
        btnNuevo = vista.findViewById(R.id.btnNuevoC);
        lvClientes = vista.findViewById(R.id.lvClientes);
    }

    void cargarDatos(){
        listaClientes = ConexionDb.getInstancia(getContext()).clienteDao().findAll();
        adapter = new ClienteAdapter(getContext(), R.layout.fila_clientes, listaClientes);
        lvClientes.setAdapter(adapter);
    }

}