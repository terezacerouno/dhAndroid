package com.idat.dreamhouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.actualizar.ActualizarDepartamentoActivity;
import com.idat.dreamhouse.actualizar.ActualizarUsuarioActivity;
import com.idat.dreamhouse.adapter.DepartamentoAdapter;
import com.idat.dreamhouse.adapter.UsuarioAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Usuario;
import com.idat.dreamhouse.nuevo.NuevoDepartamentoActivity;
import com.idat.dreamhouse.nuevo.NuevoUsuarioActivity;

import java.util.List;

public class FragmentDepartamentos extends Fragment {

    ListView lvPesonalizado;
    DepartamentoAdapter adapter;
    List<Departamento> departamentos;
    Button btnNuevo;
    ConexionDb instancia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista= inflater.inflate(R.layout.fragment_departamentos, container, false);
        instancia= ConexionDb.getInstancia(getContext());
        Enlazar(vista);
        TraerDepartamentos();
        return vista;
    }

    void Enlazar(View v){
        lvPesonalizado= v.findViewById(R.id.lvDepartamentos);
        btnNuevo= v.findViewById(R.id.btnNuevoDepartamento);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NuevoDepartamentoActivity.class);
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Departamento d= departamentos.get(position);
                Intent i = new Intent(getActivity(),
                        ActualizarDepartamentoActivity.class);
                i.putExtra("CODIGO", d.getId().intValue());
                startActivity(i);
            }
        });

        lvPesonalizado.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Departamento de= departamentos.get(position);

                AlertDialog.Builder d= new AlertDialog.Builder(getActivity());

                d.setTitle("Eliminar Departamento");
                d.setIcon(R.drawable.ic_baseline_delete_outline_24);
                d.setCancelable(false);
                d.setMessage("¿Está seguro de eliminar el departamento "+de.getId()+"?");

                d.setPositiveButton("SÍ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        instancia.daodepartamento().deletecod(de.getId().intValue());
                        Toast.makeText(getActivity(), "El departamento "+ de.getId()+" ha sido eliminado", Toast.LENGTH_SHORT).show();
                        TraerDepartamentos();
                    }
                });
                d.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Se canceló la operación", Toast.LENGTH_SHORT).show();
                    }
                });
                d.create().show();
                return  true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        TraerDepartamentos();
    }
    public void TraerDepartamentos(){
        departamentos= instancia.daodepartamento().listar();
        adapter = new DepartamentoAdapter(getContext(), R.layout.fila_departamentos, departamentos);
        lvPesonalizado.setAdapter(adapter);
    }
}