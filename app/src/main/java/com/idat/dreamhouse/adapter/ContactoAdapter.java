package com.idat.dreamhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.extras.Extras;
import com.idat.dreamhouse.model.Empleado;

import java.util.List;

public class ContactoAdapter  extends ArrayAdapter<Empleado> {

    Context micontext;
    int layout;
    List<Empleado> lista;

    public ContactoAdapter(@NonNull Context context, int resource, @NonNull List<Empleado> objects) {
        super(context, resource, objects);
        micontext=context;
        layout= resource;
        lista= objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView= LayoutInflater.from(micontext).inflate(layout, null);

        Empleado e= lista.get(position);

        TextView tv1= convertView.findViewById(R.id.txt1contacto);
        TextView tv2= convertView.findViewById(R.id.txt2contacto);
        ImageView imgA= convertView.findViewById(R.id.imgContacto);
        tv1.setText(e.getNombres()+" "+e.getA_paterno());
        tv2.setText("Teléfono: "+e.getTelefono());
        return convertView;
    }
}
