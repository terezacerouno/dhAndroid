package com.idat.dreamhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Contrato;
import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;

import java.util.List;

public class ContratoAdapter extends ArrayAdapter<Contrato> {

    Context contexto;
    int layout;
    List<Contrato> listContratos;

    public ContratoAdapter(@NonNull Context context, int resource, @NonNull List<Contrato> objects) {
        super(context, resource, objects);
        contexto = context;
        layout = resource;
        listContratos = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View vista = LayoutInflater.from(contexto).inflate(layout, null);

        TextView tvCodigo, tvEdificio, tvDepart, tvCliente, tvGarantia;

        tvCodigo = vista.findViewById(R.id.tvCodigoCo);
        tvEdificio = vista.findViewById(R.id.tvEdificioCo);
        tvDepart = vista.findViewById(R.id.tvDepartCo);
        tvCliente = vista.findViewById(R.id.tvClienteCo);
        tvGarantia = vista.findViewById(R.id.tvGarantiaCo);

        Contrato contrato = listContratos.get(position);
        Departamento departamento = ConexionDb.getInstancia(getContext()).daodepartamento().getDepartamento(contrato.getCoddepa()).get(0);
        Edificio edificio = ConexionDb.getInstancia(getContext()).daoedificio().getEdicio(departamento.getCodedificio()).get(0);
        Cliente cliente = ConexionDb.getInstancia(getContext()).clienteDao().findById(contrato.getCodcliente());

        tvCodigo.setText("ID: " + contrato.getId());
        tvEdificio.setText("Edificio: " + edificio.getDireccion());
        tvDepart.setText("Departamento: Piso N°" + departamento.getPiso());
        tvCliente.setText("Cliente: " + cliente.getNombres() + " " + cliente.getApellidos());
        tvGarantia.setText("Garantía: " + contrato.getGarantia());

        return vista;
    }
}
