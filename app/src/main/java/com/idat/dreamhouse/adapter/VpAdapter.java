package com.idat.dreamhouse.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.idat.dreamhouse.fragment.FragmentClientes;
import com.idat.dreamhouse.fragment.FragmentContratos;
import com.idat.dreamhouse.fragment.FragmentDepartamentos;
import com.idat.dreamhouse.fragment.FragmentEdificios;
import com.idat.dreamhouse.fragment.FragmentEmpleados;
import com.idat.dreamhouse.fragment.FragmentUsuarios;

public class VpAdapter extends FragmentStateAdapter {

    public VpAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment f= new Fragment();
        switch (position){
            case 0: f= new FragmentEmpleados(); break;
            case 1: f= new FragmentUsuarios(); break;
            case 2: f= new FragmentEdificios(); break;
            case 3: f= new FragmentDepartamentos(); break;
            case 4: f= new FragmentClientes(); break;
            case 5: f= new FragmentContratos(); break;
        }
        return f;
    }

    @Override
    public int getItemCount() {
        return 6;
    }
}
