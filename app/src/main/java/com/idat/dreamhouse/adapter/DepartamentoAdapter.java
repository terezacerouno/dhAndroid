package com.idat.dreamhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;

import java.util.List;

public class DepartamentoAdapter extends ArrayAdapter<Departamento> {

    Context micontext;
    int layout;
    List<Departamento> lista;
    ConexionDb instacia;

    public DepartamentoAdapter(@NonNull Context context, int resource, @NonNull List<Departamento> objects) {
        super(context, resource, objects);
        micontext=context;
        layout= resource;
        lista= objects;
        instacia= ConexionDb.getInstancia(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView= LayoutInflater.from(micontext).inflate(layout, null);

        Departamento d= lista.get(position);

        TextView tv1= convertView.findViewById(R.id.txt1departamento);
        TextView tv2= convertView.findViewById(R.id.txt2departamento);
        TextView tv3= convertView.findViewById(R.id.txt3departamento);
        TextView tv4= convertView.findViewById(R.id.txt4departamento);
        TextView tv5= convertView.findViewById(R.id.txt5departamento);
        TextView tv6= convertView.findViewById(R.id.txt6departamento);
        TextView tv7= convertView.findViewById(R.id.txt7departamento);
        TextView tv8= convertView.findViewById(R.id.txt8departamento);
        ImageView imgA= convertView.findViewById(R.id.imgDepartamento);

        Edificio e=instacia.daoedificio().getEdicio(d.getCodedificio()).get(0);
        tv1.setText("Código: "+d.getId()+"");
        tv2.setText("Dirección: "+e.getDireccion());
        tv3.setText("Piso: "+d.getPiso());
        tv4.setText("Habitaciones: "+d.getN_habitaciones());
        tv5.setText("Baños: "+d.getN_banos());
        tv6.setText("Área: "+d.getArea());
        tv7.setText("Precio: "+d.getPrecio());
        tv8.setText("Estado: "+d.getEstado());

        return convertView;
    }

}
