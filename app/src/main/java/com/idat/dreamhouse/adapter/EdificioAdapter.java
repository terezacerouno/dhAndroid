package com.idat.dreamhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.extras.Extras;
import com.idat.dreamhouse.model.Edificio;

import java.util.List;

public class EdificioAdapter extends ArrayAdapter<Edificio> {

    Context micontext;
    int layout;
    List<Edificio> lista;
    Extras extras;

    public EdificioAdapter(@NonNull Context context, int resource, @NonNull List<Edificio> objects) {
        super(context, resource, objects);
        micontext=context;
        layout= resource;
        lista= objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView= LayoutInflater.from(micontext).inflate(layout, null);
        extras= new Extras();
        Edificio e= lista.get(position);

        TextView tv1= convertView.findViewById(R.id.txt1edificio);
        TextView tv2= convertView.findViewById(R.id.txt2edificio);
        TextView tv3= convertView.findViewById(R.id.txt3edificio);
        TextView tv4= convertView.findViewById(R.id.txt4edificio);
        ImageView imgA= convertView.findViewById(R.id.imgEdificio);
        tv1.setText("Código: "+e.getId()+"");
        tv2.setText("Número de pisos: "+e.getN_pisos());
        tv3.setText("Dirección: "+e.getDireccion());
        tv4.setText("Descripción: "+e.getDescripcion());
        imgA.setImageResource(extras.obtenerIdImagen(e.getImagen(),getContext()));

        return convertView;
    }
}
