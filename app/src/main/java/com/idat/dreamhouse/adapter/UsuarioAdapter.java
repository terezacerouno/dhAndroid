package com.idat.dreamhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.extras.Extras;
import com.idat.dreamhouse.model.Usuario;

import java.util.List;

public class UsuarioAdapter extends ArrayAdapter<Usuario> {

    Context micontext;
    int layout;
    List<Usuario> lista;
    Extras extras;


    public UsuarioAdapter(@NonNull Context context, int resource, @NonNull List<Usuario> objects) {
        super(context, resource, objects);
        micontext=context;
        layout= resource;
        lista= objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView= LayoutInflater.from(micontext).inflate(layout, null);

        extras= new Extras();
        Usuario u= lista.get(position);

        TextView tv1= convertView.findViewById(R.id.txt1);
        TextView tv2= convertView.findViewById(R.id.txt2);
        TextView tv3= convertView.findViewById(R.id.txt3);
        ImageView imgA= convertView.findViewById(R.id.imgUsuario);
        tv1.setText("Código: "+u.getId_usuario()+"");
        tv2.setText("Email: "+u.getEmail());
        tv3.setText("Estado: "+u.getEstado());

        imgA.setImageResource(extras.obtenerIdImagen(u.getImagen(),getContext()));
        return convertView;
    }
}
