package com.idat.dreamhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;

import java.util.List;

public class EmpleadoAdapter extends ArrayAdapter<Empleado> {
    Context micontext;
    int layout;
    List<Empleado> lista;

    public EmpleadoAdapter(@NonNull Context context, int resource, @NonNull List<Empleado> objects) {
        super(context, resource, objects);
        micontext=context;
        layout= resource;
        lista= objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView= LayoutInflater.from(micontext).inflate(layout, null);

        Empleado e= lista.get(position);

        TextView tv1= convertView.findViewById(R.id.txt1emple);
        TextView tv2= convertView.findViewById(R.id.txt2emple);
        TextView tv3= convertView.findViewById(R.id.txt3emple);
        TextView tv4= convertView.findViewById(R.id.txt4emple);
        ImageView imgA= convertView.findViewById(R.id.imgEmpleado);
        tv1.setText("DNI: "+e.getDni()+"");
        tv2.setText(e.getNombres()+" "+e.getA_paterno()+" "+e.getA_materno());
        tv3.setText("Teléfono: "+e.getTelefono());
        tv4.setText("Estado: "+e.getEstado());

        return convertView;
    }
}
