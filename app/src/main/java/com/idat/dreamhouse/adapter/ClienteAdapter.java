package com.idat.dreamhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.idat.dreamhouse.R;
import com.idat.dreamhouse.model.Cliente;

import java.util.List;

public class ClienteAdapter extends ArrayAdapter<Cliente> {

    Context contexto;
    int layout;
    List<Cliente> listClientes;

    public ClienteAdapter(@NonNull Context context, int resource, @NonNull List<Cliente> objects) {
        super(context, resource, objects);
        contexto = context;
        layout = resource;
        listClientes = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View vista = LayoutInflater.from(contexto).inflate(layout, null);

        TextView tvCodigo, tvNombres, tvApellidos, tvDni;
        ImageView imgCliente;

        tvCodigo = vista.findViewById(R.id.tvCodigoC);
        tvNombres = vista.findViewById(R.id.tvNombresC);
        tvApellidos = vista.findViewById(R.id.tvApellidosC);
        tvDni = vista.findViewById(R.id.tvDniC);
        imgCliente = vista.findViewById(R.id.imgCliente);

        Cliente cliente = listClientes.get(position);

        tvCodigo.setText("ID: " + cliente.getId());
        tvNombres.setText("Nombres: " + cliente.getNombres());
        tvApellidos.setText("Apellidos: " + cliente.getApellidos());
        tvDni.setText("DNI: " + cliente.getDni());
        imgCliente.setImageResource(obtenerImagen(cliente.getId()));

        return vista;
    }

    int obtenerImagen(Long id){
        String nombre = "u" + id;
        String recurso = "drawable";
        String paquete = getContext().getPackageName();
        int resultado = getContext().getResources().getIdentifier(nombre, recurso, paquete);
        return resultado;
    }
}
