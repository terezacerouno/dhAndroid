package com.idat.dreamhouse.conexion;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.idat.dreamhouse.dao.ClienteDaodb;
import com.idat.dreamhouse.dao.ContratoDaodb;
import com.idat.dreamhouse.dao.DepartamentoDaodb;
import com.idat.dreamhouse.dao.EdificioDaodb;
import com.idat.dreamhouse.dao.EmpleadoDaodb;
import com.idat.dreamhouse.dao.UsuarioDaodb;
import com.idat.dreamhouse.model.Cliente;
import com.idat.dreamhouse.model.Contrato;
import com.idat.dreamhouse.model.Departamento;
import com.idat.dreamhouse.model.Edificio;
import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.model.Usuario;

@Database(entities = {Usuario.class, Empleado.class, Edificio.class
, Departamento.class, Cliente.class, Contrato.class}, version = 1)
public abstract class ConexionDb extends RoomDatabase {

    private static ConexionDb instancia;

    public abstract UsuarioDaodb dao();
    public abstract EmpleadoDaodb daoempleado();
    public abstract EdificioDaodb daoedificio();
    public abstract DepartamentoDaodb daodepartamento();
    public abstract ClienteDaodb clienteDao();
    public abstract ContratoDaodb contratoDao();

    public static ConexionDb getInstancia(Context c){

        if (instancia==null){
            instancia= Room.databaseBuilder(c.getApplicationContext(),ConexionDb.class,"dreamhouse").allowMainThreadQueries().build();
        }
        return  instancia;
    };
}
