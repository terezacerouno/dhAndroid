package com.idat.dreamhouse.conexion;

import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//No utilizar
public class ConexionWS {

    public String TraerDatos(String conexion){

        StringBuilder datos= null;
        URL url= null;
        String linea="";
        int estadodeconsultahttp=0;

        try {
            url= new URL(conexion);
            HttpURLConnection conn= (HttpURLConnection) url.openConnection();
            estadodeconsultahttp= conn.getResponseCode();
            datos= new StringBuilder();

            if (estadodeconsultahttp==HttpURLConnection.HTTP_OK){

                InputStream is= new BufferedInputStream(conn.getInputStream());
                BufferedReader br= new BufferedReader(new InputStreamReader(is));

                while ((linea= br.readLine())!=null){
                    datos.append(linea);
                }
            }

        }catch (Exception ex){
        }

        return datos.toString();
    }
}
