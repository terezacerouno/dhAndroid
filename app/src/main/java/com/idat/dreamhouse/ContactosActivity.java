package com.idat.dreamhouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.idat.dreamhouse.actualizar.ActualizarEmpleadoActivity;
import com.idat.dreamhouse.adapter.ContactoAdapter;
import com.idat.dreamhouse.adapter.EmpleadoAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;
import com.idat.dreamhouse.model.Empleado;
import com.idat.dreamhouse.nuevo.NuevoEmpleadoActivity;

import java.util.ArrayList;
import java.util.List;

public class ContactosActivity extends AppCompatActivity {

    ListView lvPesonalizado;
    ContactoAdapter adapter;
    List<Empleado> empleados;
    ConexionDb instancia;
    String accion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactos);

        instancia= ConexionDb.getInstancia(ContactosActivity.this);
        Enlazar();
        TraerEmpleados();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.manu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i=null;
        switch (item.getItemId()){
            case R.id.mnmantenimiento:
                i= new Intent(ContactosActivity.this, TabLayoutActivity.class);
                break;
            case R.id.mncontactos:
                i= new Intent(ContactosActivity.this, ContactosActivity.class);
                break;
            case R.id.mnsalir: finishAndRemoveTask();
        }
        if (i!=null){
            startActivity(i);
        }
        return true;
    }

    void Enlazar(){
        lvPesonalizado= findViewById(R.id.lvContactos);

        lvPesonalizado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Elegir(position);
            }
        });
    }

    void Elegir(int position){
        String [] act={"Llamar","Enviar SMS"};

        AlertDialog.Builder d= new AlertDialog.Builder(ContactosActivity.this);

        d.setTitle("Seleccione acción");
        d.setIcon(android.R.drawable.stat_sys_vp_phone_call_on_hold);
        d.setCancelable(false);

        d.setItems(act, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                accion= act[which];
                Empleado e= empleados.get(position);
                if (accion.equals("Llamar")){
                    Uri u= Uri.parse("tel:"+ e.getTelefono());
                    Intent i = new Intent(Intent.ACTION_CALL,u);
                    startActivity(i);
                    Toast.makeText(ContactosActivity.this, "Llamando a "+ e.getNombres(), Toast.LENGTH_SHORT).show();
                }else if (accion.equals("Enviar SMS")){
                    Intent i = new Intent(ContactosActivity.this, EnviarSMSActivity.class);
                    i.putExtra("CODIGO", e.getId_empleado());
                    startActivity(i);
                }
            }
        });

        d.setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        d.create().show();
    }

    public void TraerEmpleados(){
        empleados= instancia.daoempleado().listar();
        adapter = new ContactoAdapter(ContactosActivity.this, R.layout.fila_contactos, empleados);
        lvPesonalizado.setAdapter(adapter);
    }

}