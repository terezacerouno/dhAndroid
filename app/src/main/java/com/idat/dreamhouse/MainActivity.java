package com.idat.dreamhouse;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idat.dreamhouse.adapter.ClienteAdapter;
import com.idat.dreamhouse.adapter.ContratoAdapter;
import com.idat.dreamhouse.adapter.DepartamentoAdapter;
import com.idat.dreamhouse.adapter.EdificioAdapter;
import com.idat.dreamhouse.adapter.EmpleadoAdapter;
import com.idat.dreamhouse.adapter.UsuarioAdapter;
import com.idat.dreamhouse.conexion.ConexionDb;

public class MainActivity extends AppCompatActivity {

    int persimos_ok=100;
    String [] tablas={"Usuarios", "Empleados", "Edificios","Departamentos", "Clientes", "Contratos"};
    ArrayAdapter<String> itemAdapter1;
    AutoCompleteTextView desplegableT;
    ListView lvfiltro;
    String tablaabuscar="";
    TextInputLayout tilfiltro;
    TextInputEditText txtfiltro;
    ConexionDb instancia;
    Button btnbuscar;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ComprobarPermisos();
        instancia= ConexionDb.getInstancia(MainActivity.this);
        Enlazar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.manu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i=null;
        switch (item.getItemId()){
            case R.id.mnmantenimiento:
                i= new Intent(MainActivity.this, TabLayoutActivity.class);
                break;
            case R.id.mncontactos:
                i= new Intent(MainActivity.this, ContactosActivity.class);
                break;
            case R.id.mnsalir:
                finishAndRemoveTask();
                i= new Intent(MainActivity.this, LoginActivity.class);
                break;
        }
        if (i!=null){
            startActivity(i);
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void ComprobarPermisos(){
        int permisosms= ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS);
        int permisollamada= ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE);

        if (permisosms!= PackageManager.PERMISSION_GRANTED || permisollamada!=PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.CALL_PHONE
            }, persimos_ok);
        }else{
            Toast.makeText(this, "Pemisos concedidos", Toast.LENGTH_SHORT).show();
        }
    }

    void Enlazar(){
        desplegableT= findViewById(R.id.itemtablas);
        lvfiltro= findViewById(R.id.lvfiltro);
        tilfiltro= findViewById(R.id.tilBuscar);
        txtfiltro= findViewById(R.id.txtbuscar);
        btnbuscar=findViewById(R.id.btnBuscar);
        itemAdapter1 = new ArrayAdapter<String>(MainActivity.this, R.layout.listaitems, tablas);
        desplegableT.setAdapter(itemAdapter1);

        desplegableT.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tablaabuscar=(String)parent.getItemAtPosition(position);
                desplegableT.setHint(tablaabuscar);
            }
        });

        btnbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tablaabuscar.equals("Usuarios")){
                    UsuarioAdapter ua= new UsuarioAdapter(MainActivity.this,R.layout.fila_usuarios,instancia.dao().filtrado(txtfiltro.getText().toString()));
                    lvfiltro.setAdapter(ua);
                }else if(tablaabuscar.equals("Empleados")){
                    EmpleadoAdapter ea= new EmpleadoAdapter(MainActivity.this,R.layout.fila_empleados,instancia.daoempleado().filtrado(txtfiltro.getText().toString()));
                    lvfiltro.setAdapter(ea);
                }else if(tablaabuscar.equals("Edificios")){
                    EdificioAdapter eda= new EdificioAdapter(MainActivity.this,R.layout.fila_edificios,instancia.daoedificio().filtrado(txtfiltro.getText().toString()));
                    lvfiltro.setAdapter(eda);
                }else if(tablaabuscar.equals("Departamentos")){
                    DepartamentoAdapter da= new DepartamentoAdapter(MainActivity.this,R.layout.fila_departamentos,instancia.daodepartamento().filtrado(txtfiltro.getText().toString()));
                    lvfiltro.setAdapter(da);
                } else if(tablaabuscar.equals("Clientes")){
                    ClienteAdapter ca = new ClienteAdapter(MainActivity.this, R.layout.fila_clientes, instancia.clienteDao().filtrado(txtfiltro.getText().toString()));
                    lvfiltro.setAdapter(ca);
                } else if(tablaabuscar.equals("Contratos")){
                    ContratoAdapter coa = new ContratoAdapter(MainActivity.this, R.layout.fila_contratos, instancia.contratoDao().filtrado(txtfiltro.getText().toString()));
                    lvfiltro.setAdapter(coa);
                } else{
                    Toast.makeText(MainActivity.this, "Seleccione un item de la lista", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}