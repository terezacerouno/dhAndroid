package com.idat.dreamhouse.extras;

import android.content.Context;

public class Extras {

    public Extras() {
    }

    public int obtenerIdImagen (String img, Context c){
        String nom_imagen =  img;
        String recurso = "drawable";
        String paquete = c.getPackageName();
        int resultado = c.getResources().getIdentifier(nom_imagen, recurso, paquete);
        return resultado;
    }
}
